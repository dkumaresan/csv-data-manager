require 'test_helper'

class CsvDataViewerControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get csv_data_viewer_index_url
    assert_response :success
  end

end
