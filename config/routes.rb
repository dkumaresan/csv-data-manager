Rails.application.routes.draw do
  get 'csv_data_viewer/index'
  resources :articles
  root 'csv_data_viewer#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
