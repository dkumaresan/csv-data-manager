# README

1. Install Ruby
2. Install Ruby on Rails - rails
3. Install dependencies Clone the project and go to the directory, 'bundle install'
4. cp config/AdWords-API-Location-Criteria-2017-11-02.csv /tmp/
5. ./bin/rails server
6. Go to browser http://localhost:3000

#TODO:
1. Persist CSV data to In memmory database like Redis (https://github.com/redis/redis-rb)
2. Perisist CSV file checksum which can act as a version for the data (say reading,parsing, persisting csv data can be avoided repeatedly and at the same time
 can be re-loaded if change in checksum i.e data)
4. Implement search through the redis server with sorting
5. Page can be loaded from the data in the redis server as paginated. say when n being the number of items in a page, 1 to n, n+1 to 2n, and so.
