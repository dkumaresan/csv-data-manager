class CsvDataViewerController < ApplicationController
  attr_accessor :data
  def index
     @data=[]
     total_chunks = SmarterCSV.process('/tmp/AdWords-API-Location-Criteria-2017-11-02.csv', {:force_utf8=>true,:chunk_size => 2}) do |chunk|
     #Criteria ID,Name,Canonical Name,Parent ID,Country Code,Target Type,Status
     #:criteria_id , :name , :canonical_name, :parent_id, :country_code, :target_type, :status
	chunk.each do |h|   
          @data << h  
	end
     end
  end
end
